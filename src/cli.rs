mod commands;
use commands::Command;

use clap::Parser;

#[derive(Parser)]
#[command(version, about, author, long_about = None)]
pub struct Cli {
    /// Sets the log output level to Trace and changed some other things to make debugging easier.
    #[arg(long, default_value_t = false, global = true)]
    debug: bool,

    /// Prevents non-essential output. For use in APIs.
    #[arg(short, long, default_value_t = false, global = true, conflicts_with = "verbose")]
    quiet: bool,

    /// Format output in a more human-readable way.
    #[arg(short, long, default_value_t = false, global = true, conflicts_with = "quiet")]
    verbose: bool,

    /// Prevent all network activity and run from local cache.
    #[arg(long, default_value_t = false, global = true)]
    offline:bool,

    #[command(subcommand)]
    command: Command,
}
