use clap::{Args, Subcommand};

#[derive(Args)]
pub struct Ping {
    #[command(subcommand)]
    command: PingCommands,
}

#[derive(Subcommand)]
enum PingCommands {
    Server,
    Content,
}