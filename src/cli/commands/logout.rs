use clap::Args;

#[derive(Args)]
pub struct Logout {
    /// Formated as "USERNAME@SERVER.TLD"
    account: String
}