use clap::Args;

#[derive(Args)]
pub struct Fetch {
    /// What do you want to download?
    contentID: String,

    /// Where do you want to download it?
    #[arg(short, long, requires = "contentID")]
    save: Option<String>,
}