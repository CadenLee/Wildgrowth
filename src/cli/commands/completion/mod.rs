use clap::{Args,Subcommand};

#[derive(Args)]
pub struct Completion {
    #[command(subcommand)]
    command: Shell,
}

#[derive(Subcommand)]
enum Shell {
    Fish,
    ZSH,
    Bash,
    Powershell,
}