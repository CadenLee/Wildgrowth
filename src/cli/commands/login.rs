use clap::Args;

#[derive(Args)]
pub struct Login {
    ///Formated as "USERNAME@SERVER.TLD"
    account: String,
}