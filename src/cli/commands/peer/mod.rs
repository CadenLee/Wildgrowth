use clap::{Args, Subcommand};

#[derive(Args)]
pub struct Peer {
    #[command(subcommand)]
    command: PeerCommands,
}

#[derive(Subcommand)]
enum PeerCommands {
    Add,
    Remove,
}