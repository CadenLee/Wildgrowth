use clap::Args;

#[derive(Args)]
pub struct Init {
    name: Option<String>,
}