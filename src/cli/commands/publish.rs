use clap::Args;

#[derive(Args)]
pub struct Publish {
    ///
    contentID: String,

    /// Formated as "USERNAME@SERVER.TLD"
    account: String,

    ///
    #[arg(short, long)]
    encrypt: bool,

    ///
    #[arg(short, long, requires = "encrypt")]
    password: Option<String>
}