use clap::{Args, Subcommand};

#[derive(Args)]
pub struct Config {
    #[command(subcommand)]
    command: ConfigCommands,
}

#[derive(Subcommand)]
enum ConfigCommands {
    Set(Set),
    Unset(UnSet),
}

#[derive(Args)]
struct Set {

}

#[derive(Args)]
struct UnSet {
    
}