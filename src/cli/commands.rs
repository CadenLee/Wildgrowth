mod init; mod login; mod logout; mod account; mod config;
mod search; mod publish; mod fetch; mod info; mod ping;
mod peer; mod sync; mod completion;

use clap::Subcommand;

#[derive(Subcommand)]
pub enum Command {
    /// Perform initial setup actions.
    Init(init::Init),

    /// Resets local configuration to fresh install.
    Reset,

    /// Login to a remote Wildgrowth server.
    Login(login::Login),

    /// Logout from a remote Wildgrowth server.
    Logout(logout::Logout),

    /// Manage accounts on remote servers.
    Account(account::Account),

    /// Set local configuration parameters. 
    Config(config::Config),

    /// Display status of local wildgrowth service.
    Status,

    /// Attempt to find issues in local configuration.
    Doctor,

    /// Search connected servers and local instance for content.
    Search(search::Search),

    /// Encrypts and uploads a content id's file to remote server.
    Publish(publish::Publish),

    /// Retrieve files from content id into local cache. Outputs to stdout. 
    Fetch(fetch::Fetch),

    /// Display available metadata attached to a content id.
    Info(info::Info),

    /// Diagnose and display networking information.
    Ping(ping::Ping),

    /// Manage peers.
    Peer(peer::Peer),

    /// Manage networking actions.
    Sync(sync::Sync),

    /// Attempt to perform a self-update of the wildgrowth-cli.
    Update,

    /// Display licensing information.
    License,

    /// Generate shell auto-completion. Use 'wildgrowth help completion' to learn more.
    Completion(completion::Completion),
}
